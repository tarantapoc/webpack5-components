import React from 'react';
import ReactDOM from 'react-dom';
import AttributeDisplay from './AttributeDisplay';
import { StoreProvider } from 'store/store';

const App = () => (
  <div>
    <StoreProvider>
      <AttributeDisplay />
      <div>Name: AttributeDisplay</div>
    </StoreProvider>
  </div>
);
ReactDOM.render(<App />, document.getElementById('app'));
