import React from "react";
import { useStore } from 'store/store';

let old = Date.now();

const AttributeDisplay = () => {
  const { value } = useStore();
  const diff = Date.now() - old;
  old = Date.now();

  return (
      <div>{value.length}:Diff: {String(diff).padStart(2, '0')} ms Value: {value[value.length - 1]?.value.toFixed(2).padStart(2, '0')}</div>
  );
};

export default AttributeDisplay;
