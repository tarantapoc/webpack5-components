import React from "react";
import ReactDOM from "react-dom";
import ProgressBar from './ProgressBar'

import "./index.css";

const App = () => (
  <div className="container">
    <ProgressBar />
  </div>
);
ReactDOM.render(<App />, document.getElementById("app"));
