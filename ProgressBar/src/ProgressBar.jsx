import React, { useState, useEffect, useRef } from 'react';

const ProgressBar = () => {
  const intervalRef = useRef(0);
  const [value, setValue] = useState(0);

  // Start the interval
  const startInterval = () => {
    if (intervalRef.current !== 0) return;
    intervalRef.current = window.setInterval(() => {
      setValue((prevValue) => {
        if (prevValue >= 100) return 0;
        else return prevValue + 1;
      });
    }, 100);
  };

  // Stop the interval
  const stopInterval = () => {
    if (intervalRef.current) {
      window.clearInterval(intervalRef.current);
      intervalRef.current = 0;
    }
  };

  // Use the useEffect hook to cleanup the interval when the component unmounts
  useEffect(() => {
    // here's the cleanup function
    return () => {
      if (intervalRef.current !== null) {
        stopInterval();
      }
    };
  }, []);

  startInterval();

  return (
    <div className="progress-div" style={{ width: '250px' }}>
      {value}
      <div
        style={{
          width: `${value}px`,
          backgroundColor: 'rgb(235, 122, 62)',
          height: '10px',
          borderRadius: '1rem',
        }}
      />
    </div>
  );
};

export default ProgressBar;
