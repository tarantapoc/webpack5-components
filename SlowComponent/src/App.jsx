import React from 'react';
import ReactDOM from 'react-dom';

import SlowComponent from './SlowComponent';

import './index.scss';

const App = () => (
  <div className="mt-10 text-3xl mx-auto max-w-6xl">
    <SlowComponent />
    <div>Name: SlowComponent</div>
  </div>
);
ReactDOM.render(<App />, document.getElementById('app'));
