import React, { useEffect, useState } from 'react';

const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const SlowComponent = () => {
  const [buttonName, setButtonName] = useState(String(Math.random()));

  const handleClick = async () => {
    setButtonName(String(Math.random()));
  };

  useEffect(() => {
    async function makeRequest() {
      console.log('before');

      await delay(5000);

      console.log('after');
    }

    makeRequest();
  });

  const now = Date.now();
  const n = [...Array(5999999).keys()].reduce((p, c) => p + c);
  console.log('Reduce takes: ', Date.now() - now, ' ms');

  return (
    <div>
      <button onClick={handleClick}>
        {buttonName} Click {n}
      </button>
    </div>
  );
};

export default SlowComponent;
