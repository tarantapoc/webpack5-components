import React from 'react';
import ReactDOM from 'react-dom';
import CountDisplay from './CountDisplay';
import CountAdd from './CountAdd';
import CountClear from './CountClear';

import './index.css';

import { StoreProvider } from 'counterStore/counterStore';

const App = () => (
  <StoreProvider>
    <div className="container">
      <CountDisplay />
      <CountAdd />
      <CountClear />
    </div>
  </StoreProvider>
);
ReactDOM.render(<App />, document.getElementById('app'));
