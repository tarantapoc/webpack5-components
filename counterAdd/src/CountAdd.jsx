import React from 'react';

import { useStore } from 'counterStore/counterStore';

const CountAdd = () => {
  const { count, increment } = useStore();
  return (
    <div className="text-3xl mx-auto max-w-6xl">
      <button onClick={increment}>Add 1</button>
    </div>
  );
};

export default CountAdd;
