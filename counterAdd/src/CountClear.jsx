import React from 'react';

import { useStore } from 'counterStore/counterStore';

const CountReset = () => {
  const { clear } = useStore();
  return (
    <div className="text-3xl mx-auto max-w-6xl">
      <button onClick={clear}>Reset</button>
    </div>
  );
};

export default CountReset;
