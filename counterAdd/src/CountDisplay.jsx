import React from 'react';

import { useStore } from 'counterStore/counterStore';

const CountDisplay = () => {
  const { count } = useStore();
  return (
    <div className="text-3xl mx-auto max-w-6xl">
      <div>Count: {count}</div>
    </div>
  );
};

export default CountDisplay;
