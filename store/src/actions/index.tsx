import { DATA } from '../constants';

const loadData = (pattern: number) => ({
    type: DATA.LOAD,
    pattern
});

const setData = (data: number) => ({
    type: DATA.LOAD_SUCCESS,
    data,
});

const setDataError = (error: string) => ({
    type: DATA.LOAD_FAIL,
    error,
});


export {
    loadData,
    setData,
    setDataError,
};
