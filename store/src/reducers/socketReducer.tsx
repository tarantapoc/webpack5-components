import { WEBSOCKET } from '../constants';
import { ActionInt } from '../types/types';
import config from '../config.json';

const socketReducer = (state = [], action: ActionInt) => {
    //console.log("socketReducer: ", state, action);
    switch (action.type) {
        case WEBSOCKET.WS_MESSAGE: {
            const result = action.value.map(item => {
                const data = JSON.parse(item.data);
                const { payload: { data: { attributes } } } = data;
                const { device, attribute, value, writeValue, timestamp } = attributes;
                return ({ device, attribute, value, writeValue, timestamp })
            });
            if (state.length <= (config.historyLimit-1)) {
                return [...state, result[0]];
            }
            else {
                state = state.slice(-(config.historyLimit-1));
                return [...state, result[0]];
            }
        }
        default:
            return state;
    }
};

export default socketReducer;
