import { put, call, take, fork } from 'redux-saga/effects';
import { setData, setDataError } from '../actions/index';
import { DATA } from '../constants';
import { queryData } from '../api';

export function* handleDataLoad(pattern: number) {
    try {
        const data: number = yield call(queryData, pattern);
        yield put(setData(data));
    } catch (error: unknown) {
        if (typeof error === "string") {
            yield put(setDataError(error));
        } else if (error instanceof Error) {
            yield put(setDataError(error.message));
        }
    }
}

export default function* watchFunction() {
    while (true) {
        const { pattern } = yield take(DATA.LOAD);
        yield fork(handleDataLoad, pattern);
    }
}
