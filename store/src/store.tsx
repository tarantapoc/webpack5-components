import React from "react";
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { websocketMiddleware } from './websocketMiddleware';
//import { createLogger } from "redux-logger";
import rootSaga from './sagas';
import rootReducer from './reducers';
import { Provider, useSelector } from 'react-redux';
import { RootState } from '../src/types/types';


declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const configureStore = () => {
    const sagaMiddleware = createSagaMiddleware();
    //const loggerMiddlware = createLogger();
    const store = createStore(
        rootReducer,
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
            ? compose(
                applyMiddleware(websocketMiddleware(), sagaMiddleware/*,loggerMiddlware*/),
                window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__(),
            )
            : applyMiddleware(websocketMiddleware(), sagaMiddleware/*,loggerMiddlware*/),
    );
    sagaMiddleware.run(rootSaga);
    return store;
};

export default configureStore;

export function useStore() {
    const value = useSelector((state: RootState) => state.socket);
    // const value = 15;
    return { value };
}

export function StoreProvider({ children }) {
    return <Provider store={configureStore()}> { children }</Provider>
}
