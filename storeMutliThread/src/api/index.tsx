import config from '../config.json';

async function queryData(pattern: number) {

    if (pattern) {
        await sleep(config.data_delay);
        const timestamp = Date.now();
        return [pattern, timestamp];
    }
    else return [];
}

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export { queryData };
