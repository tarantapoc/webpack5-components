import { MiddlewareAPI } from 'redux';
import config from './config.json';
import { ActionInt } from './types/types';
import { WEBSOCKET } from './constants';

const fullNames = ["sys/tg_test/1/double_scalar", "test/webjivetestdevice/1/RandomAttr"];
const ATTRIBUTES = `
    subscription Attributes($fullNames: [String]!) {
        attributes(fullNames: $fullNames) {
        device
        attribute
        value
        writeValue
        timestamp
        }
    }`;

function socketUrl(): string {
    const loc = window.location;
    const protocol = loc.protocol.replace("http", "ws");
    return protocol + "//" + config.websocket + "/" + config.tangoDB + "/socket?dashboard";
}

export const websocketMiddleware = () => {

    return (store: MiddlewareAPI<any, any>) => {// eslint-disable-line
        const ws = socketUrl();
        const socket = new (WebSocket as any)(ws, "graphql-ws");// eslint-disable-line

        if (socket) {
            socket.addEventListener("open", () => {
                const variables = { fullNames };
                const startMessage = JSON.stringify({
                    type: "start",
                    payload: {
                        query: ATTRIBUTES,
                        variables
                    }
                });
                console.log(startMessage);
                socket.send(startMessage);
                store.dispatch({ type: WEBSOCKET.WS_CONNECTED });
            });

            socket.addEventListener("disconnected", () => {
                store.dispatch({ type: WEBSOCKET.WS_DISCONNECTED })
            });
            socket.addEventListener("message", (message: MessageEvent) => {
                store.dispatch({ type: WEBSOCKET.WS_MESSAGE, value: [message] });
                console.log(message);
            });
        }
        else console.log("Socket error");

        return (next: (action: ActionInt) => void) => (action: ActionInt) => {
            //console.log("Socket Action:", action);
            if (
                action.type &&
                action.type === WEBSOCKET.WS_SEND_MESSAGE &&
                socket.readyState === 1
            ) {
                console.log(WEBSOCKET.WS_SEND_MESSAGE);
            }

            return next(action);
        };
    };
};