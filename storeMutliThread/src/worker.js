import { createStore,applyMiddleware } from 'redux';
import rootreducer from './reducers';
import createSagaMiddleware from 'redux-saga';
import { expose, createProxyStore } from 'react-redux-worker';
import { websocketMiddleware } from './websocketMiddleware';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootreducer,
  applyMiddleware(websocketMiddleware(), sagaMiddleware/*,loggerMiddlware*/)) // if you have initial state and/or middleware you can add them here as well
const proxyStore = createProxyStore(store);
sagaMiddleware.run(rootSaga);
expose(proxyStore, self);